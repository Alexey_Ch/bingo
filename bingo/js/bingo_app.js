angular.module('bingoApp', []).
    service('timerService', function() {
        var timers = [];

        this.addTimer = function(funcName, time) {
            var timer = setInterval(funcName, time);
            timers.push(timer);
        }

        this.deleteTimers = function() {
            for(var i = 0; i < timers.length; i++) {
                clearInterval(timers[i]);
            }
        }
    }).
    service('roomService', function() {
        var rooms = [
            {id:1, title:'room 1'},
            {id:2, title:'room 2'},
            {id:3, title:'room 3'},
            {id:5, title:'room 4'},
            {id:6, title:'room 5'},
            {id:7, title:'room 6'},
            {id:8, title:'room 7'}
            ];


        this.getRoomList = function() {
            return rooms;
        }

    }).
    service('playersService', function() {
        var rooms = [
            {id:1, title:'player 1'},
            {id:2, title:'player 2'},
            {id:3, title:'player 3'},
            {id:5, title:'player 4'},
            {id:6, title:'player 5'},
            {id:7, title:'player 6'},
            {id:8, title:'player 7'}
        ];


        this.getRoomList = function() {
            return rooms;
        }

    }).
    service('gameService', function() {
        var GAME_CONFIG = (function() {
            var private = {
                'MIN_VALUE': 1,
                'MAX_VALUE': 60,
                'CELL_AMOUNT_X':5,
                'CELL_AMOUNT_Y':5
            };

            return {
                get: function(name) { return private[name]; }
            };
        })();

        var games = [];

        var usedNumbers = [];

        var field = [];

        function isWin(field, usedNumbers) {
            var result = false;
            var middleX = getMiddleX();
            var middleY = getMiddleY();
            var row;
            // 1 - free cell in center of card
            if(usedNumbers.length >= (GAME_CONFIG.get('CELL_AMOUNT_X') * GAME_CONFIG.get('CELL_AMOUNT_Y')) - 1) {
                console.log("Lenght > 24 ");
                for(var y = 0; y < field.length; y++) {
                    row = field[y];
                    for(var x = 0; x < row.length; x++) {
                        if(usedNumbers.indexOf(row[x].value) == -1 && (y != middleY && x != middleX)) {
                            console.log("FALSE: arr item: " + (y*5 + x) + ", value: " + row[x].value) ;
                            return false;
                        }
                    }
                }
                result = true;
            }
            console.log("RESULT: " + result) ;
            return result;
        }

        function getRandomInt(min, max) {
            return Math.floor(Math.random() * (max - min + 1)) + min;
        }

        function getRandomOne() {
            return 1;
        }

        function getMiddleX() {
            return Math.floor(GAME_CONFIG.get('CELL_AMOUNT_X') / 2);
        }

        function getMiddleY() {
            return Math.floor(GAME_CONFIG.get('CELL_AMOUNT_Y') / 2);
        }

        function generateField() {
            var field = new Array();
            var row;
            var cell;
            console.log("CELL_AMOUNT_Y: " + GAME_CONFIG.get('CELL_AMOUNT_Y'));
            console.log("CELL_AMOUNT_X: " + GAME_CONFIG.get('CELL_AMOUNT_X'));
            for(var y = 0; y < GAME_CONFIG.get('CELL_AMOUNT_Y'); y++) {
                console.log("ROW: " + y);
                row = new Array();;
                for(var i = 0; i < GAME_CONFIG.get('CELL_AMOUNT_X'); i++) {
                    console.log("COLUMN: " + i);
                    cell = new Object();
                    if((i == getMiddleX()) && (y == getMiddleY())) {
                        console.log(" MIDDLE ");
                        cell.value = "FREE";
                    } else {
                        cell.value = getRandomOne();/*getRandomInt(GAME_CONFIG.get('MIN_VALUE'), GAME_CONFIG.get('MAX_VALUE'));*/
                    }
                    cell.x = i;
                    cell.y = y;
                    cell.selected = "cell";
                    console.log("CELL[" + cell.value + "]");
                    row.push(cell);
                }
                field.push(row);
                console.log("--------------------------------");
                console.log(field);
            }
            return field;
        }

        function isContain(field, number) {
            var result = new Object();
            result.status = false;
            var row;
            for(var i = 0; i < field.length; i++) {
                row = field[i];
                for(var j = 0; j < row.length; j++) {
                    if(row[j].value == number) {
                        result.status = true;
                        result.x = j;
                        result.y = i;
                        return result;
                    }
                }
            }
            return result;
        }

        function gameLoop(game) {

            console.log('GAME LOOP');
            console.log('GAME ID: ' + game.id);
            var number = getNextNumber(game.usedNumbers);
            console.log('GAME [' + game.id + '] next: ' + number);
            var result = isContain(game.field, number);
            console.log('GAME [' + game.id + '] isContain: ' + result.status);

            if(result.status == true) {
                console.log('GAME [' + game.id + '] x, y: ' + result.x + ', ' + result.y);
                console.log('GAME [' + game.id + '] animate: ');
                console.log('CELL: ' + '.cell_' + result.x + '_' + result.y);
                game.field[result.y][result.x].selected = "selected";

            }
            console.log('GAME [' + game.id + '] field');
            console.log(game.field);
            console.log('GAME [' + game.id + '] usedNumbers');
            console.log(game.usedNumbers);

            if(isWin(game.field, game.usedNumbers)) {
                console.log('GAME [' + game.id + '] WIN: ');
                clearInterval(game.timer);
            } else {
                console.log('GAME [' + game.id + '] PLAY YET: ');
            }
        }

        this.getFieldModel = function() {
            field = generateField();
            return field;

        }

         function getNextNumber(usedNumbers) {
            var max = GAME_CONFIG.get('MAX_VALUE');
            var nextNumber = getRandomInt(GAME_CONFIG.get('MIN_VALUE'), GAME_CONFIG.get('MAX_VALUE'));
            while(usedNumbers.length <  max && usedNumbers.indexOf(nextNumber) != -1) {
                nextNumber = getRandomInt(GAME_CONFIG.get('MIN_VALUE'), GAME_CONFIG.get('MAX_VALUE'));
            }
            if(usedNumbers.length < max) {
                usedNumbers.push(nextNumber);
            }

            return nextNumber;
        }

        this.getUsedNumbers = function() {
            return usedNumbers;
        }

        this.checkWin = function(gameId) {
            return isWin(field, usedNumbers);
        }

        this.createGame = function(roomId) {
            console.log("createGame with id: " + roomId);
            var game = new Object();
            game.field = generateField();
            game.usedNumbers = new Array();
            game.id = roomId;
            games.push(game);
            game.timer = setInterval( function() { gameLoop(game); }, 2000 );
            return game;
        }
    }).
    controller('BingoCtrl', ['$scope', 'roomService', 'timerService', function ($scope, roomService, timerService) {
        timerService.deleteTimers();
        $scope.rooms = roomService.getRoomList();
        $scope.message = "Hello, World!";
        $scope.message2 = "Hello, from bingo room!";

    }]).
    controller('FieldCtrl',['$scope', 'gameService', 'timerService', function ($scope, gameService, timerService) {
        var game = gameService.createGame(1);

        $scope.rows = game.field;

        /*function generateNextNumber() {
            var nextNumber = gameService.getNextNumber();
            console.log("next number: " + nextNumber);
            var used = gameService.getUsedNumbers();
            console.log("Used: "  + used);
            console.log("Used length: "  + used.length);

            console.log("Win: " + gameService.checkWin(0));

        }

        timerService.addTimer(generateNextNumber, 2000);*/


    }])
    .
    config(['$routeProvider', function($routeProvider) {
        $routeProvider.
            when('/', {templateUrl: 'partials/list.html', controller:'BingoCtrl'}).
            when('/room/:id', {templateUrl: 'partials/room.html', controller: 'FieldCtrl'}).
            otherwise('/');
    }]);
